'use strict';

$(document).ready(function() {
	// Mobile menu
	function openMobMenu() {
		let btn = $('#mob-menu');
		let menu = $('#header');

		btn.on('click', function() {
			$(this).toggleClass('mob-menu_active');
			menu.toggleClass('header_active');
		});
	}
	openMobMenu();

	// Calendar
	$('#calendar').datepicker({
		autoClose: true
	});

	function appendCalendar() {
		let calendar = $('#datepickers-container');
		let hasCalendar = calendar.length;
		let calendarWrap = $('#calendar').parent();
		let days = calendar.find('.datepicker--days');
		let calendarInner = calendar.find('.datepicker');

		if (hasCalendar >= 1) {
			calendar.appendTo(calendarWrap);
			days.on('click', function() {
				calendarInner.removeClass('active');
				return false;
			});
		}
	}
	appendCalendar();

	// PopUp
	function openPopUp() {
		$('.js-popup-button').on('click', function() {
			$('.popup').removeClass('js-popup-show');
			let popupClass = '.' + $(this).attr('data-popupShow');
			$(popupClass).addClass('js-popup-show');
			if ($(document).height() > $(window).height()) {
				let scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
				$('html').addClass('noscroll').css('top', -scrollTop);
			}
		});
		closePopup();
	}

	// Close PopUp
	function closePopup() {
		$('.js-close-popup').on('click', function() {
			$('.popup').removeClass('js-popup-show');
			let scrollTop = parseInt($('html').css('top'));
			$('html').removeClass('noscroll');
			$('html, body').scrollTop(-scrollTop);
			return false;
		});
		
		$('.popup').on('click', function(e) {
			let div = $('.popup__wrap');
			let datePicker = $('#datepickers-container').length;
			
			if (!div.is(e.target) && div.has(e.target).length === 0 && datePicker == 0) {
				$('.popup').removeClass('js-popup-show');
				let scrollTop = parseInt($('html').css('top'));
				$('html').removeClass('noscroll');
				$('html, body').scrollTop(-scrollTop);
			}
		});
	}
	openPopUp();
	

	// Masked Phone
	$('input[type="tel"]').mask('+7(999)999-99-99');

	// Scroll
	$('#personal-info').find('.table-wrap__inner').scrollbar();
	$('#main').find('.winners-table__wrap').scrollbar();

	// Tabs
	function openTabs() {
		let wrap = $('#personal-info');
		let tabsContent = wrap.find('.table-wrap');
		let tabsBtn = wrap.find('.tabs li');

		// Reset
		tabsBtn.eq(0).addClass('active');
		tabsContent.eq(0).addClass('active');

		// Tabs Main Action
		wrap.find('.tabs').on('click', 'li', function() {
			let i = $(this).index();

			tabsBtn.removeClass('active');
			$(this).addClass('active');
			tabsContent.removeClass('active');
			tabsContent.eq(i).addClass('active');
		});
	}
	openTabs();

	// Select
	$('select').select2();

	// Jquery Validate
	function checkValidate() {
		let form = $('form');

		$.each(form, function() {
			$(this).validate({
				ignore: [],
				errorClass: 'error',
				validClass: 'success',
				rules: {
					name: {
						required: true,
						letters: true
					},
					surname: {
						required: true,
						letters: true
					},
					secondname: {
						required: true,
						letters: true
					},
					serial: {
						required: true,
						letters: true
					},
					email: {
						required: true,
						email: true
					},
					phone: {
						required: true,
						phone: true
					},
					city: {
						required: true,
						letters: true
					},
					captcha: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					password: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					home: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					apartment: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					number: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					inn: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					textarea: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					textarea1: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					calendar: {
						required: true,
						normalizer: function(value) {
							return $.trim(value);
						}
					},
					fileupload1: {
						required: true, 
						filesize: 5048576,   
					},
					fileupload2: {
						required: true,
						filesize: 5048576,   
					},
					fileupload3: {
						required: true, 
						filesize: 5048576,   
					}
				},
				messages: {
					phone: 'Некорректный номер',
					rules: {
						required: ''
					},
					personal: {
						required: ''
					},
					fileupload1: {
						required: ''
					},
					fileupload2: {
						required: ''
					},
					fileupload3: {
						required: ''
					},
					
				},
			});

			jQuery.validator.addMethod('letters', function(value, element) {
				return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
			});

			jQuery.validator.addMethod('email', function(value, element) {
				return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
			});

			jQuery.validator.addMethod('phone', function(value, element) {
				return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
			});

			jQuery.validator.addMethod('filesize', function(value, element, param) {
				return this.optional(element) || (element.files[0].size <= param);
			}); 

		});
	}
	checkValidate();

	// Download-photo
	function downloadPhoto() {
		window.URL = window.URL || window.webkitURL;

		let wrap = $('#type-files');
		let inputFile = wrap.find('input[type="file"]');

		inputFile.on('change', function() {
			let $this = $(this);
			let photo = $this.siblings('.file-wrap__label').find('.file-wrap__photo');
			let icon = photo.find('.icon-camera');
			let btnClose = $this.parent().find('.delete-photo');

			function handleFiles(files) {
				if (!files.length) {
					photo.removeAttr('style').removeClass('file-wrap__photo_uploaded');
					icon.removeClass('icon-camera_none');
					$this.removeAttr('img').val('');
					btnClose.removeClass('delete-photo_active');
				} else {
					for (let i = 0; i < files.length; i++) {
						let getImagePath = window.URL.createObjectURL(files[i]);
						photo.css('background-image', 'url(' + getImagePath + ')').addClass('file-wrap__photo_uploaded');
						icon.addClass('icon-camera_none');
						$this.attr({
							'img' : getImagePath
						});
						btnClose.addClass('delete-photo_active');

						btnClose.on('click', function() {
							photo.removeAttr('style').removeClass('file-wrap__photo_uploaded');
							icon.removeClass('icon-camera_none');
							$this.removeAttr('img').val('');
							btnClose.removeClass('delete-photo_active');
						});

						return;			
					}
				}
			}
			handleFiles(this.files);
		});
	}
	downloadPhoto();

	// Define mobile
	function defineMob() {
		let wrap = $('#header').find('.discription-att');
		let define = detect.parse(navigator.userAgent);
		let os = define.os.family;

		if (os == 'iOS') {
			wrap.addClass('discription-att_ios');
		} else if (os == 'Android') {
			wrap.addClass('discription-att_android');
		}
	}
	defineMob();

	// Choose prize
	function choosePrize() {
		const select = $('#choose-prize');
		const img = select.closest('.form__popup').find('.form__img img');

		select.on('select2:select', function(e) {
			let data = e.params.data.id;
			img.get(0).src = data;
		});
	}
	choosePrize();
});